terraform {
  backend "gcs" {
    bucket      = "vl-dev-serverless"
    prefix      = "tfstate"
    credentials = "vl-dev-test-sa.json"
  }
}
