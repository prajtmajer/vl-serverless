# define GCP region
variable "gcp_region" {
  type        = string
  description = "GCP region"
}

# define GCP domain
variable "gcp_project_domain" {
  type        = string
  description = "GCP project domain"
}

# define GCP project id
variable "gcp_project_id" {
  type        = string
  description = "GCP project id"
}

# GCP authentication file
variable "gcp_auth_file" {
  type        = string
  description = "GCP authentication file"
}

variable "storage_class" {
  type        = string
  description = "The storage class of the Storage Bucket to create"
}

variable "service_account_terraform" {
  type        = string
  description = ""
}

variable "ssl_private_key_path" {
  type        = string
  description = ""
}

variable "ssl_certificate_path" {
  type        = string
  description = ""
}

# https://learn.hashicorp.com/tutorials/terraform/sensitive-variables
variable "sendgrid_api_key" {
  description = "Sendgrid e-mail API key"
  type        = string
  sensitive   = true
}