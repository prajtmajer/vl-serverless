gcp_project_id="vl-dev-serverless"
gcp_project_domain="www.valuelogic.dev"
gcp_region="europe-central2"
gcp_auth_file="vl-dev-test-sa.json"
service_account_terraform="vl-dev-test-sa"
storage_class="REGIONAL"
ssl_private_key_path="~/www/vl-dev/ssl/private.key"
ssl_certificate_path="~/www/vl-dev/ssl/certificate.crt"
