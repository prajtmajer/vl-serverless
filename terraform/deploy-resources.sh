#!/usr/bin/env bash
set -o allexport; source secrets.env; set +o allexport
terraform init
terraform apply