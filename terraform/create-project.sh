#!/usr/bin/env bash
set -o allexport; source terraform.tfvars; set +o allexport

gcloud init
ACTIVE_PROJECT_ID=$(gcloud config list --format 'value(core.project)' 2>/dev/null)
gcloud projects describe ${ACTIVE_PROJECT_ID}

gcloud alpha billing accounts projects link ${ACTIVE_PROJECT_ID} --billing-account=$(pass gcp_billing_accout_id)

gcloud iam service-accounts create ${service_account_terraform} \
    --display-name "${ACTIVE_PROJECT_ID} Terraform" \
    --description="${ACTIVE_PROJECT_ID} Terraform Infrastructure Service Account"

gcloud projects add-iam-policy-binding ${ACTIVE_PROJECT_ID} \
    --member="serviceAccount:${service_account_terraform}@${ACTIVE_PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/storage.admin"

gcloud iam service-accounts keys create ${service_account_terraform}.json \
    --iam-account ${service_account_terraform}@${ACTIVE_PROJECT_ID}.iam.gserviceaccount.com

gsutil mb -c regional -l ${gcp_region} gs://${ACTIVE_PROJECT_ID}
