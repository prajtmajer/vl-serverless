data "google_project" "tf-vl-dev-project" {
  project_id = var.gcp_project_id
}

resource "google_project_service" "project_service_cloudfunctions" {
  project = var.gcp_project_id
  service = "cloudfunctions.googleapis.com"
}

resource "google_project_service" "project_service_cloudbuild" {
  project = var.gcp_project_id
  service = "cloudbuild.googleapis.com"
}

resource "google_project_service" "project_service_compute" {
  project = var.gcp_project_id
  service = "compute.googleapis.com"
}

resource "google_project_service" "project_service_datastore" {
  project = var.gcp_project_id
  service = "datastore.googleapis.com"
}

resource "google_storage_bucket" "static-site" {
  project = var.gcp_project_id
  name          = var.gcp_project_domain
  location      = "EUROPE-CENTRAL2"
  force_destroy = true
  storage_class = "REGIONAL"

  website {
    main_page_suffix = "index.html"
    not_found_page   = "404.html"
  }
}

resource "google_storage_bucket_access_control" "static-site-public-rule" {
  bucket = google_storage_bucket.static-site.name
  role   = "READER"
  entity = "allUsers"
}

resource "google_storage_bucket_object" "archive" {
  name   = "cloud-function-source.zip"
  bucket = google_storage_bucket.static-site.name
  source = "../artifacts/gcp/cloud-function-source.zip"
}

# backend connected with backend (domain name)
resource "google_compute_backend_bucket" "lb_bucket_backend" {
  project     = var.gcp_project_id
  bucket_name = var.gcp_project_domain
  name        = "vl-dev-lb-backend"
  description = "VL Dev load balancer backend"
  enable_cdn  = false

  depends_on = [
    google_storage_bucket_access_control.static-site-public-rule
  ]
}

# routing to bucket service
resource "google_compute_url_map" "website_url_map" {
  project         = var.gcp_project_id
  name            = "website-url-map"
  description     = "Website url map - backend"
  default_service = google_compute_backend_bucket.lb_bucket_backend.self_link
}


resource "google_compute_ssl_certificate" "vl_ssl_cert" {
  project = var.gcp_project_id
  name        = "vl-dev-cert-ssl"
  private_key = file(var.ssl_private_key_path)
  certificate = file(var.ssl_certificate_path)

  lifecycle {
    create_before_destroy = true
  }
}

# target http proxy used by forwarding rule
resource "google_compute_target_https_proxy" "website_https_proxy" {
  project = var.gcp_project_id
  name    = "vl-dev-lb-fronend"

  ssl_certificates = [google_compute_ssl_certificate.vl_ssl_cert.id]
  url_map = google_compute_url_map.website_url_map.self_link
}

# public address ip
resource "google_compute_global_address" "website_public_address" {
  project      = var.gcp_project_id
  name         = "lb-public-ip"
  ip_version   = "IPV4"
  address_type = "EXTERNAL"
}

# forwarding rule ip to target proxy
resource "google_compute_global_forwarding_rule" "website_global_forwarding_rule" {
  project    = var.gcp_project_id
  name       = "lb-forwarding-rule"
  target     = google_compute_target_https_proxy.website_https_proxy.self_link
  ip_address = google_compute_global_address.website_public_address.address
  port_range = "443"
}

resource "google_cloudfunctions_function" "function" {
  project = var.gcp_project_id
  name        = "newsletter_sign"
  description = "writes data to datastore, send e-mail"
  runtime     = "python37"
  region = var.gcp_region

  available_memory_mb   = 256
  source_archive_bucket = google_storage_bucket.static-site.name
  source_archive_object = google_storage_bucket_object.archive.name
  trigger_http          = true
  entry_point           = "newsletter_sign"
  environment_variables = {
    SENDGRID_API_KEY = var.sendgrid_api_key
  }
}

# IAM entry for all users to invoke the function
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.function.project
  region         = google_cloudfunctions_function.function.region
  cloud_function = google_cloudfunctions_function.function.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}

resource "google_app_engine_application" "app" {
  project = var.gcp_project_id
  location_id = var.gcp_region
  database_type = "CLOUD_DATASTORE_COMPATIBILITY"
}