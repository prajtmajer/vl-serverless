#!/bin/bash
echo "Copying build to Storage"
gsutil defacl ch -u AllUsers:READER gs://$1
#gsutil rsync -R static/ gs://$1
gsutil rsync -R dist/ gs://$1
gsutil setmeta -h "Cache-Control:private, max-age=0, no-transform" gs://$1/*
