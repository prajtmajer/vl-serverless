import json
import logging
import os
import pandas as pd
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail, Email
from python_http_client.exceptions import HTTPError
from google.cloud import datastore

# If this is running in a cloud function, then GCP_PROJECT should be defined
if 'GCP_PROJECT' in os.environ:
    PROJECT_ID = os.environ['GCP_PROJECT']
# else if this is running locally then GOOGLE_APPLICATION_CREDENTIALS should be defined
elif 'GOOGLE_APPLICATION_CREDENTIALS' in os.environ:
    with open(os.environ['GOOGLE_APPLICATION_CREDENTIALS'], 'r') as fp:
        credentials = json.load(fp)
        PROJECT_ID = credentials['project_id']
else:
    raise Exception('Failed to determine project_id')


"""
    https://console.cloud.google.com/marketplace/product/sendgrid-app/sendgrid-email
"""


def send_email(email):
    message = Mail(
        from_email='prajtmajer@valuelogic.one',
        to_emails=email,
        subject='Serverless with GCP',
        html_content='Frontend repo: https://github.com/pdobrowolskivalue/vl-dev-frontend<br /> '
                     'GCP Serverless backend repo: https://bitbucket.org/prajtmajer/vl-serverless<br />'
                     'AWS Serverless frontend repo: https://github.com/bkopanczyk/zg-demo-frontend<br />'
                     'AWS Serverless backend repo: https://github.com/bkopanczyk/zg-demo-backend<br />'
    )
    sg = SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))
    response = sg.send(message)
    return response.status_code


"""Responds to any HTTP request.
    Args:
        request (flask.Request): HTTP request object.
    Returns:
        The response text or any set of values that can be turned into a
        Response object using
        `make_response <https://flask.palletsprojects.com/en/1.1.x/api/#flask.Flask.make_response>`.
    """


def newsletter_sign(request):
    # Set CORS headers for the preflight request
    if request.method == 'OPTIONS':
        # Allows GET requests from any origin with the Content-Type
        # header and caches preflight response for an 3600s
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }

        return '', 204, headers

    # Set CORS headers for the main request
    headers = {
        'Access-Control-Allow-Origin': '*'
    }

    request_json = request.get_json()

    datastore_client = datastore.Client()
    transaction = datastore_client.transaction()
    transaction.begin()
    q = datastore_client.query(kind='user')
    q.add_filter('email', '=', request_json['email'])

    result_user = q.fetch()
    list(result_user)

    if result_user.num_results > 0:
        transaction.rollback()
        return 'Duplicate', 400, headers
    else:
        task_key = datastore_client.key("user", request_json['email'])
        task = datastore.Entity(key=task_key)
        task["email"] = request_json['email']
        task["address"] = request_json['address']
        task["name"] = request_json['name']
        task["surname"] = request_json['surname']
        task["termsAndConditionsAcceptance"] = request_json['termsAndConditionsAcceptance']
        datastore_client.put(task)
        transaction.commit()
        send_email(request_json['email'])

    return 'OK', 200, headers
