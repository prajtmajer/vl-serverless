
## VL Serverless

#### Deployment

###### Project creation
```bash
terraform/create-project.sh
```
###### Resources deployment
```bash
terraform/deploy-resources.sh
```
###### Application deployment
```bash
deploy-app.sh www.valuelogic.dev
```

#### Sensitive variables

Values for passwords etc are stored locally using and then they are assigned to environment variables, for example
```bash
TF_VAR_sendgrid_api_key="$(pass tf_sendgrid)"
```
https://www.passwordstore.org/